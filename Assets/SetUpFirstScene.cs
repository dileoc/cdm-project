﻿using UnityEngine;
using System.Collections;

public class SetUpFirstScene : MonoBehaviour {

	public GameObject[] thingsToDisable = new GameObject[2];
	public GameObject[] thingsToEnable = new GameObject[2];


	void Start(){
		DontDestroyOnLoad(gameObject);
	}
	// Use this for initialization
	void OnLevelWasLoaded(int lvl){
		if (lvl == 0){
			Debug.Log("Firing Setter");
			//thingsToDisable = GameObject.FindGameObjectsWithTag("Video1");
			foreach (GameObject go in thingsToDisable){
				go.SetActive(false);
			}
			foreach (GameObject go in thingsToEnable){
				go.SetActive(true);
			}
			Debug.Log("Seppuku");
			gameObject.SetActive(false);

			if (GameObject.FindGameObjectWithTag("BackgroundMusic"))
				Destroy(GameObject.FindGameObjectWithTag("BackgroundMusic"));
		}
	}
}
