﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChooseCarArrowScript : MonoBehaviour {

	public GameObject[] pics = new GameObject[3];
	public int currentPic = 0;
	public GameObject continueButton;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void nextPic(){
		if (currentPic < pics.Length - 1){
			pics[currentPic].SetActive(false);
			currentPic++;
			pics[currentPic].SetActive(true);
		}
		continueButton.GetComponent<Button>().interactable = (currentPic == 0);
	}

	public void prevPic(){
		if (currentPic > 0){
			pics[currentPic].SetActive(false);
			currentPic--;
			pics[currentPic].SetActive(true);
		}
		continueButton.GetComponent<Button>().interactable = (currentPic == 0);
	}
}
