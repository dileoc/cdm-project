﻿using UnityEngine;
using System.Collections;

public class RaycastEnable : MonoBehaviour {

	// enable or disable car rotation based on raycast hit to vehicle
	public bool rotEnabled = false;
	public GameObject script;
	private Ray ray;
	RaycastHit hit;

	void Update () {
		if (Input.GetButtonDown("Fire1")){
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			//Debug.Log(Input.mousePosition);

			if (Physics.Raycast(ray, out hit)){
				//Debug.Log(ray + " hit " + hit);

				if(hit.collider.tag == "Player"){
					rotEnabled = true;
					//Debug.Log("Hit player");

				}
			}
		} else if (Input.GetButtonUp("Fire1")){
			//Debug.Log("Off Player");

			rotEnabled = false;
		}

		GetComponent<MouseLook>().enabled = rotEnabled;
	}
}
