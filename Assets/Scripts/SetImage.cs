﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetImage : MonoBehaviour {

	public void setImageTo(Sprite tex){
		gameObject.GetComponent<Image>().sprite = tex;
		gameObject.GetComponent<Image>().color = new Color(1,1,1,1);
	}

}
