﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RawImagePlayMovie : MonoBehaviour {
	private MovieTexture movTex;
	public GameObject[] thingsToTrigger = new GameObject[2];
	
	// Use this for initialization
	void Start () {
		movTex = (MovieTexture) gameObject.GetComponent<RawImage>().texture;
		movTex.Play();
	}

	// Update is called once per frame
	void Update () {
		if (!movTex.isPlaying){
			foreach (GameObject go in thingsToTrigger){
				go.SetActive(true);
			}
			gameObject.SetActive(false);
		}
	}
}
