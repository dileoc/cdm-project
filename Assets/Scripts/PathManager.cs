﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PathManager : MonoBehaviour {
	
	public List<iTweenEvent> events;
	public int pathNo = 0;
	public ArrayList evts = new ArrayList(4);

	// Use this for initialization
	void Start () {
		// disable itween events except first
		findAllTweenEvents();
		//enablePath(pathNo);
	}

	public void nextPath() {
		enablePath(pathNo);
		if (pathNo < events.Count - 1){
			pathNo++;
		}
	}

	public void enablePath(int no){
		Debug.Log("Entering enablePath");

		for (int i = 0; i < events.Count; i++){
			events[i].enabled = false;
		}
		events[no].enabled = true;
		Debug.Log("Playing path no. " + events[no].tweenName);
		events[no].Play();
	}

	private void findAllTweenEvents(){
		events = gameObject.GetComponents<iTweenEvent>().OrderBy(go => go.tweenName).ToList();
		for (int i = 0; i < events.Count; i++){
			Debug.Log(events[i].tweenName);
		}
	}

}
