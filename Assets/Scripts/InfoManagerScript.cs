﻿using UnityEngine;
using System.Collections;

public class InfoManagerScript : MonoBehaviour {

	public string userName;
	public string vehicleChoice;
	public string extColour;
	public string intColour;
	public string drivingLocation;
	public string musicChoice;
	public float drivingScore;
	public float questionScore;
	public string enteredHomeAddress;
	public string homePostalCode;
	public string enteredDestAddress;
	public string destPostalCode;
	public float dailyCommuteDistance;
	public float calculatedDailyConsumption;
	public string email;

	void Start(){
		DontDestroyOnLoad(gameObject);
	}

	public void setUserName(string un){
		userName = un;
	}
	public void setVehicle(string vehicle){
		vehicleChoice = vehicle;
	}
	public void setExtColour(string colour){
		extColour = colour;
	}
	public void setIntColour(string colour){
		intColour = colour;
	}
	public void setMusicChoice(string choice){
		musicChoice = choice;
	}
	public void setDrivingScore(float score){
		drivingScore = score;
	}
	public void setQuestionScore(float score){
		questionScore += score;
	}
	public void setDrivingLocation(string location){
		drivingLocation = location;
	}
	public void setOriginPostCode(string code){
		homePostalCode = code;
	}
	public void setDestPostCode(string code){
		destPostalCode = code;
	}
	public void setEmail(string email){
		this.email = email;
	}
}
