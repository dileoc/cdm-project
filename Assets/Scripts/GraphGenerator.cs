﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GraphGenerator : MonoBehaviour {

	public float dailyCommute;
	public InfoManagerScript infoManager;

	public int numYears = 5;
	public GameObject[,] graphBars;
	public float[,] graphHeights;


	private const int DAYS_YR = 365;
	private const int WORK_DAYS_YR = 262;

	private float parentHeight,parentWidth;
	private float LPer100KmRateCity = 8.7f;
	private float LPer100KmRateHwy = 8.4f;

	// Use this for initialization
	void Start () {
		graphBars = new GameObject[numYears,2];
		graphHeights = new float[numYears,2];

		if (infoManager == null){
			infoManager = (InfoManagerScript) GameObject.Find("InfoManager").GetComponent<InfoManagerScript>();
		}
		dailyCommute = infoManager.dailyCommuteDistance;
	}

	public void GenerateGraph(){
		for (int i = 0; i < numYears; i++){
			graphHeights[i,0] = dailyCommute * WORK_DAYS_YR * i;
			// TODO: finish implementation
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
