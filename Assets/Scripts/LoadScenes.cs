﻿using UnityEngine;
using System.Collections;

public class LoadScenes : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void loadCustom_1() {
        Application.LoadLevel(1);
    }

    public void loadOutSide_2() {
        Application.LoadLevel(2);
    }

    public void loadInside() {
        Application.LoadLevel(3);
    }

    public void loadDriving() {
        Application.LoadLevel(5);
    }

    public void backToTitleInfiveSec() {
        Invoke("back", 5.0f);    
    }

    private void back() {
        Application.LoadLevel(0);
    }
}
