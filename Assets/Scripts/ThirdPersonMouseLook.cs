﻿using UnityEngine;
using System.Collections;

public class ThirdPersonMouseLook : MonoBehaviour {

	public Transform rotateTarget;
	public float rotSpeed = 10;


	// Update is called once per frame
	void Update () {
		float rot = Time.deltaTime * rotSpeed * Input.GetAxis("Mouse X");
		transform.RotateAround(rotateTarget.position, Vector3.up,  rot);
	}
}
