﻿using UnityEngine;
using System.Collections;

public class HideAfterSeconds : MonoBehaviour {

	public int timeToHide = 5;

	// Use this for initialization
	void Start () {
		Invoke("HideMe", timeToHide);
	}

	void HideMe(){
		gameObject.SetActive(false);
	}

}
