﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChooseButtons : MonoBehaviour {

    public GameObject[] showImgObj;

    private Image[] imgCmp;
    private int index = 0;
    private int length = 0;
	// Use this for initialization
	void Start () {
        length = showImgObj.Length;
        imgCmp = new Image[length];
        for (int a = 0; a < length; a++)
        {
            imgCmp[a] = showImgObj[a].GetComponent<Image>();
            
        }
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void preCar() {
        
        index--;
        if(index == -1){
            index = length - 1;
        }
        showImg(index);
    }

    public void nextCar() {
        index++;
        if(index == length){
            index = 0;
        }
        showImg(index);
    }

    private void showImg(int _index) {
        for (int a = 0; a < length; a++ )
        {
            imgCmp[a].enabled = false;
        }

        imgCmp[index].enabled = true;
    }
}
