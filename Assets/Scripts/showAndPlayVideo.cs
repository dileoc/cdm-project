﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class showAndPlayVideo : MonoBehaviour {

	// changed setup so that referenced objects are actually the parent of the surface that holds the movie tex
	public GameObject[] playingFrames = new GameObject[3];
	public SceneControllerScript controller;
	public int currentSurface = 0;
	private MovieTexture movTex;
	public int[] movieDuration = {22,21,23}; 
	public int delayToShow = 2;

	void Start(){
	}

	public void playMovie(){
		Invoke("playM", delayToShow);
	}

	private void hidePlayingSurface(){
		Debug.Log("Hide Playing Surface");
		// Hide the in-game video surface and begin next leg of journey right away
		playingFrames[currentSurface].SetActive(false);
		// hide the parent videopanel, too
		Debug.Log("Hiding " + playingFrames[currentSurface].transform.parent.gameObject + " too");
		playingFrames[currentSurface].transform.parent.gameObject.SetActive(false); //wow is that an ugly line.
		gameObject.GetComponent<SpeedControl>().enabled = true;
		currentSurface++;
	}

	private void playM(){
		// In order: set Parent Vid panel, self active
		playingFrames[currentSurface].transform.parent.gameObject.SetActive(true);
		playingFrames[currentSurface].SetActive(true);
		// Grab movie texture from child, play it, call delayed hide
		movTex = (MovieTexture) playingFrames[currentSurface].GetComponentInChildren<RawImage>().texture;
		movTex.Play();
		Invoke("hidePlayingSurface", movieDuration[currentSurface]);
	}

}
