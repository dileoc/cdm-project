﻿using UnityEngine;
using System.Collections;

public class ShowAfterDelay : MonoBehaviour {
	
	public GameObject[] toShow = new GameObject[1];
	public AudioSource[] toPlay = new AudioSource[1];
	public float[] delayTime = new float[1];
	public int currentShowIndex = 0;
	public bool willShowFirst = true;

	// Use this for initialization
	void Start () {
		if (willShowFirst) showNext();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void showNext(){
		Invoke("showNow", delayTime[currentShowIndex]);
	}

	void showNow(){
		toShow[currentShowIndex].SetActive(true);
		if (toPlay[currentShowIndex] != null){
			toPlay[currentShowIndex].Play();
		}
		currentShowIndex++;	
	}
}
