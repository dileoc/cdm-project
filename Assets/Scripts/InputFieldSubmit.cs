﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputFieldSubmit : MonoBehaviour {

	public GameObject target;
	public string fieldContents;
	public GameObject fieldContainer;

	// So much duplication - TODO: come back and pretty this up.
	void Start(){
		if (target == null){
			target = GameObject.Find("InfoManager");
		}
	}
	public void SubmitUserName(){
		fieldContents = gameObject.GetComponent<InputField>().textComponent.text;
		Debug.Log("Submitting " + fieldContents);
		target.SendMessage("setUserName", fieldContents);
	}
	public void SubmitVehicle(string choice){
		Debug.Log("Submitting " + choice);
		target.SendMessage("setVehicle", choice);
	}
	public void SubmitDrivingLocation(string choice){
		Debug.Log("Submitting " + choice);
		target.SendMessage("setDrivingLocation", choice);
	}
	public void SubmitExtColour(string choice){
		Debug.Log("Submitting " + choice);
		target.SendMessage("setExtColour", choice);
	}
	public void SubmitIntColour(string choice){
		Debug.Log("Submitting " + choice);
		target.SendMessage("setIntColour", choice);
	}
	public void SubmitMusicChoice(string choice){
		Debug.Log("Submitting " + choice);
		target.SendMessage("setMusicChoice", choice);
	}
	public void SubmitDrivingScore(float num){
		Debug.Log("Submitting " + num);
		target.SendMessage("setDrivingScore", fieldContents);
	}
	public void SubmitQuestionScore(float num){
		Debug.Log("Submitting " + num);
		target.SendMessage("setQuestionScore", fieldContents);
	}
	public void SubmitHomePost(){
		string code = GameObject.FindGameObjectWithTag("OriginInputBox").GetComponent<InputField>().textComponent.text;
		Debug.Log("Submitting " + code);
		target.SendMessage("setOriginPostCode", code);
	}
	public void SubmitDestPost(){
		string code = GameObject.FindGameObjectWithTag("DestInputBox").GetComponent<InputField>().textComponent.text;
		Debug.Log("Submitting " + code);
		target.SendMessage("setDestPostCode", code);
	}
	public void SubmitEmail(){
		fieldContents = fieldContainer.GetComponent<InputField>().textComponent.text;
		Debug.Log("Submitting " + fieldContents);
		target.SendMessage("setEmail", fieldContents);
	}
}
