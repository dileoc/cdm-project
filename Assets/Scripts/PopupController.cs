﻿using UnityEngine;
using System.Collections;

public class PopupController : MonoBehaviour {
	
	// to store question/correct answer/incorrect answer text
	public GameObject [] text = new GameObject[3];

	void Start(){
		showText(0);
		Camera.main.GetComponent<MouseDownEnable>().DisableMouseLook();
	}

	public void showText(int textChoice){
		foreach (GameObject go in text){
			go.SetActive(false);
		}
		text[textChoice].SetActive(true);
	}

	public void showPanel(){

	}

	public void hidePanel(){

	}
}
