﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ShowColorText : MonoBehaviour {

	// Use this for initialization
    void Awake() {
        gameObject.GetComponent<Text>().enabled = false;
    }

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void wakeup() {
        gameObject.GetComponent<Text>().enabled = true;
    }

    public void sleep() {
        gameObject.GetComponent<Text>().enabled = false;
    }
}
