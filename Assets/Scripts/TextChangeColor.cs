﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextChangeColor : MonoBehaviour {

    private Text text;
    public GameObject gameObj;
    void Awake() {
        text = gameObj.GetComponent<Text>();
    }
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void changeColorToBlue() {
        
        text.color = new Color(0.2f, 0.45f, 0.75f);
        
    }

    public void changeColorToWhite() {
        text.color = Color.white;
    }
}
