﻿using UnityEngine;
using System.Collections;

public class dontDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DestroyInSecs(){
		Invoke("JustKidding", 4);
	}
	void JustKidding(){
		GameObject.Destroy(gameObject);
	}
}
