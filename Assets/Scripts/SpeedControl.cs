﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpeedControl : MonoBehaviour {
	public GameObject sceneController;

	int numPaths = 4;

	public int speedMultiplier = 5;
	public float speed = 0f;
	public float maxSpeed = 5f;
	public float absTopSpeed = 5f;
	public float pointToStartSlow = .85f;
	
	public bool forceDecel = false;
	public bool isOnAuto = false;
	
	float distance = 0f;
	public SortedList<int, Vector3[]> paths = new SortedList<int, Vector3[]>();
	Vector3[] currentPath;
	int currentPathNo = 0;

	public float pathLength;
	public float perc = 0;
	string pathname;
	
	void Start()
	{
		for (int i = 0; i < numPaths; i++){
			pathname = "CityPath"+ i;
			currentPath = iTweenPath.GetPath(pathname);
			paths.Add(i, currentPath);
		}
		SetPath (currentPathNo);
		ResetPathVars();
	}

	void SetPath(int pathNo){
		if (pathNo < numPaths){
			currentPath = paths[pathNo];
			pathLength = iTween.PathLength(currentPath);
		}
	}

	void ResetPathVars(){
		speed = 0;
		maxSpeed = absTopSpeed;
		distance = 0;
		perc = 0;
	}

	void Update()
	{
		if (perc < pointToStartSlow){

		} else if (perc < .99) {
			Decel();
		} else {
			// We're done, fire off the end state here
			if (currentPathNo < numPaths){
				// trigger popups, disable until popups return control
				sceneController.GetComponent<SceneControllerScript>().displayPopups();
				gameObject.GetComponent<SpeedControl>().enabled = false;
				SetPath(++currentPathNo);
				ResetPathVars();
			}
		}
		speed = Mathf.Clamp(speed + (Input.GetAxis ("Vertical") * speedMultiplier * Time.deltaTime),
		                    0f, maxSpeed);
		distance += speed;
		perc = distance / pathLength;
		iTween.PutOnPath (gameObject, currentPath, perc);
		this.transform.LookAt (iTween.PointOnPath (currentPath, perc + 0.01f));
	}
	
	void Decel(){
		// ease max speed to zero, fire stop events manually
		float x = (pointToStartSlow - perc)/(1f - pointToStartSlow);
		maxSpeed = (.5f * (Mathf.Cos(x / (Mathf.PI / 10f)) + 1)) * absTopSpeed;
	}
	
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "SlowZone"){
			forceDecel = true;
		}
	}
}
