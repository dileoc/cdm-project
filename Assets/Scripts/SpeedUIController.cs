﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeedUIController : MonoBehaviour {

	public GameObject needle;
	public GameObject speedObj;
	public GameObject controllerParent;
	public GameObject fuelGaugeParent;
	public Transform needleRotation;

	private RectTransform needleTrans;
	private Text speedText;
	private SpeedControl controller;
	private RectTransform fuelGauge;
	public float fuel = 1f; // change to private when done tweaking
	public float fullFuelBurn = .001f;
	public float halfFuelBurn = .0001f;
	public float actualFuelBurn = 0;
	private float fullGasPoint = 70f;
	private float halfGasPoint = 40f;

	private int speedConversionRate = 20;
	private int speedTodisplay;
	private float fuelGaugeHeight;



	// Use this for initialization
	void Start () {
		needleTrans = needle.GetComponent<RectTransform>();
		speedText = speedObj.GetComponent<Text>();
		controller = controllerParent.GetComponent<SpeedControl>();
		fuelGauge = fuelGaugeParent.GetComponent<RectTransform>();
		fuelGaugeHeight = fuelGauge.rect.height;
	}
	
	// Update is called once per frame
	void Update () {
		UpdateSpeed();
		//UpdateNeedle();
	}

	void UpdateSpeed(){
		speedTodisplay = Mathf.FloorToInt((controller.speed) * speedConversionRate);
		speedText.text = speedTodisplay.ToString();
		// are we burning gas?
		if (Input.GetAxisRaw("Vertical") > .9 || speedTodisplay > halfGasPoint){
			actualFuelBurn = (speedTodisplay > fullGasPoint) ? fullFuelBurn : halfFuelBurn;
			fuel = Mathf.Clamp(fuel - actualFuelBurn * Time.deltaTime, 0, 1);
		} else {
			actualFuelBurn = 0;
		}
		fuelGauge.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, fuelGaugeHeight * fuel);
	}

	void UpdateNeedle(){
		if (Input.GetAxisRaw("Vertical") > 0){
			needle.transform.RotateAround(needleTrans.position, Vector3.back, Input.GetAxisRaw("Vertical"));
		}
	}
}