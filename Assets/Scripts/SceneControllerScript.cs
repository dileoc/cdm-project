﻿using UnityEngine;
using System.Collections;

public class SceneControllerScript : MonoBehaviour {

	public Color extColour = Color.white;
	public Color intColour = Color.white;
	public Material extMaterial;
	public Material intMaterial;
	public int popToShow = 0;
	public GameObject[] popupPanels = new GameObject[4];
	public AudioSource[] audioClips = new AudioSource[4];
	public GameObject vehicle;
	public int alpha = 90;

	private PathManager pManager;
	private Color[] extColours = new Color[9];

	void Start(){
		// there *has* to be a better way to specify RGBA in int values...
		extColours[0] = new Color	(15/255f,15/255f,15/255f,alpha/255f);
		extColours[1] = new Color	(51/255f,64/255f,122/255f,alpha/255f);
		extColours[2] = new Color	(209/255f,198/255f,179/255f,alpha/255f);
		extColours[3] = new Color	(130/255f,136/255f,129/255f,alpha/255f);
		extColours[4] = new Color	(174/255f,173/255f,178/255f,alpha/255f);
		extColours[5] = new Color	(240/255f,240/255f,240/255f,alpha/255f);
		extColours[6] = new Color 	(95/255f,47/255f,56/255f,alpha/255f);
		extColours[7] = new Color 	(191/255f,191/255f,193/255f,alpha/255f);
		extColours[8] = new Color	(65/255f, 65/255f, 65/255f);

		pManager = vehicle.GetComponent<PathManager>();
	}
	
	public void changeScene(int sceneToSwitchTo){
		Application.LoadLevel(sceneToSwitchTo);
	}

	public void changeExtColour(int colourNum){
		extColour = extColours[colourNum];
		extMaterial.SetColor("_Color", extColour);
	}

	public void changeIntMaterial(int colourNum){
		intColour = extColours[colourNum];
		intMaterial.SetColor("_Color", intColour);
	}

	public void displayPopups(){
		popupPanels[0].GetComponent<ShowAfterDelay>().showNext();
//		if (popupPanels[popToShow] != null){
//			popupPanels[popToShow].gameObject.SetActive(true);
			audioClips[popToShow].Play();
			popToShow++;
//		}
	}

	public void putOnNextPath(){
		pManager.nextPath();
	}

	public void playFirstPath(){
		Debug.Log("Playing First Path");
		pManager.enablePath(0);
	}

	public void toggleBackgroundMusic(){
		GameObject bg = GameObject.FindGameObjectWithTag("BackgroundMusic");
		if (bg !=null){
			bg.SetActive(!bg.activeSelf);
		}
	}
}
