﻿using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections;
using System.Text.RegularExpressions;

public class GoogleMapsDirectionsScript : MonoBehaviour {
	private string API_KEY = "AIzaSyD6HQrN3qzOTl4rtT9mnXTowrlIHNwWI0Q";
//	private string canadianPostalCodeRegex = 	"[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] " +
//													"?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]";
	public float distance;
	public InputField originInput, destInput;
	public Text distanceOutput, destCodeOutput, originCodeOutput;

	// Use this for initialization
	void Start () {
		/*
		Debug.Log("starting");
		StartCoroutine(GetDirections("M4E2N8", "V6T1X1"));
		Debug.Log("Ending");
		*/
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GetDirections(){
		string orig = originInput.textComponent.text;
		string dest = destInput.textComponent.text;
		orig = orig.Replace(" ", "+");
		dest = dest.Replace(" ", "+");
		StartCoroutine(GetGoogleDirections(orig, dest));
		Debug.Log("query sent to google");
	}

	// will retrieve JSON object from gmaps
	private IEnumerator GetGoogleDirections(string origin, string destination){
		string url = "https://maps.googleapis.com/maps/api/directions/json?" +
				"origin=" + origin + "&" +
				"destination=" + destination + "&" +
				"key=" + API_KEY;

		Debug.Log("Waiting for request to return");
		WWW www = new WWW(url);
		yield return www;

		if (www.error != null){
			Debug.Log(www.error);
		}else{
			Debug.Log("Parsing: " + www.text);
			JSONNode parsedData = JSONNode.Parse(www.text);
			Debug.Log("Completed Parse");
			Debug.Log("The total distance is: " + parsedData["routes"][0]["legs"][0]["distance"]["value"].AsInt);

			distance = parsedData["routes"][0]["legs"][0]["distance"]["value"].AsInt;
			distanceOutput.text = distance/1000 + " km";

			// extract postal codes

		}

	}
}
