﻿using UnityEngine;
using System.Collections;

public class SummarySceneManagerScript : MonoBehaviour {
	
	public GameObject[] panels = new GameObject[4];
	public int currentPanel = 0;
	
	public GameObject retryBtn;
	public GameObject backBtn;
	public GameObject continueBtn;

	public GameObject setterUpperForRestart;

	void Start(){
		switchToPanel(0);
		setterUpperForRestart = Instantiate(setterUpperForRestart, Vector3.zero, Quaternion.identity) as GameObject;
		setterUpperForRestart.SetActive(true);
		//toggleBackgroundMusic();
	}
	
	private void switchToPanel(int p){
		foreach (GameObject go in panels){
			go.SetActive(false);
		}
		panels[p].SetActive(true);
	}
	
	public void nextPanel(){
		if (currentPanel < panels.Length - 1)
		{
			currentPanel++;
			if (currentPanel == 1)
			{
				retryBtn.SetActive(false);
				backBtn.SetActive(true);
			}
			switchToPanel(currentPanel);
			
			if(currentPanel == panels.Length - 1){
				continueBtn.SetActive(false);
			}
		}
	}
	
	public void prevPanel(){
		if (currentPanel > 0){
			currentPanel--;
			if (currentPanel == 0)
			{
				retryBtn.SetActive(true);
				backBtn.SetActive(false);
			}
			
			if (currentPanel == panels.Length - 2)
			{
				continueBtn.SetActive(true);
				
			}
			
			switchToPanel(currentPanel);
		}
	}

	public void toggleBackgroundMusic(){
		GameObject bg = GameObject.FindGameObjectWithTag("BackgroundMusic");
		if (bg !=null){
			bg.SetActive(!bg.activeSelf);
		}
	}
}
