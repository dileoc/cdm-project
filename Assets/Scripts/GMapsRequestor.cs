﻿using UnityEngine;
using System.Collections;

public class GMapsRequestor : MonoBehaviour {

	public string targetURL = "https://www.google.ca/maps/@49.266883,-123.089981,3a,90y,9.77h,70.26t/data=" +
		"!3m5!1e1!3m3!1saUn6-qRNNM8AAAQIt_DEMQ!2e0!3e2?hl=en";

	public int width = 200;
	public int height = 200;
	public float sideHeading = 0;
	public float sidePitch = 0;
	public float fieldOfView = 90f;

	private string key = "AIzaSyD6HQrN3qzOTl4rtT9mnXTowrlIHNwWI0Q";


	// Use this for initialization
	void Start () {
		Debug.Log("Starting");
		StartCoroutine(GetStreetviewTexture(49.266883,-123.089981, 0, 0));
		Debug.Log("called Get");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*
	 * Taken whole-cloth from http://forum.unity3d.com/threads/google-streetview-in-oculus-rift-hmd.182867/#post-1371539
	 * Not for release!
	 */
	private IEnumerator GetStreetviewTexture(double latitude, double longitude, double heading, double pitch = 0.0)
	{          
		Debug.Log("entered method");
		string url = "http://maps.googleapis.com/maps/api/streetview?"
			+ "size=" + width + "x" + height
				+ "&location=" + latitude + "," + longitude
				+ "&heading=" + (heading + sideHeading) % 360.0 + "&pitch=" + (pitch + sidePitch) % 360.0  
				+ "&fov=" + fieldOfView+ "&sensor=false";
		
		if (key != "")
			url += "&key=" + key;
		Debug.Log("creating a www object");
		
		WWW www = new WWW(url);
		Debug.Log("waiting for WWW return");
		yield return www;

		if (!string.IsNullOrEmpty(www.error))
			Debug.Log("Panorama " + name + ": " + www.error);
		else
			print("Panorama " + name + " loaded url " + url);
		
		GetComponent<Renderer>().material.mainTexture = www.texture;
	}
}
