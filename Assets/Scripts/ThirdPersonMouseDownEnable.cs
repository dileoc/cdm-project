﻿using UnityEngine;
using System.Collections;

public class ThirdPersonMouseDownEnable : MonoBehaviour {

	// enable or disable car rotation based on raycast hit to vehicle
	public bool rotEnabled = false;
	public GameObject script;
	
	void Update () {
		if (Input.GetButton("Fire1")){
			rotEnabled = true;
		} else if (Input.GetButtonUp("Fire1")){
			rotEnabled = false;
		}
		
		GetComponent<ThirdPersonMouseLook>().enabled = rotEnabled;
	}
}
