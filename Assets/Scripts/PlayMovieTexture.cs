﻿using UnityEngine;
using System.Collections;

public class PlayMovieTexture : MonoBehaviour {

	private MovieTexture movTex;

	// Use this for initialization
	void Start () {
		movTex = (MovieTexture) GetComponent<Renderer>().material.mainTexture;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Jump")){
			movTex.Play();
		}
	}
}
