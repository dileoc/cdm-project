﻿using UnityEngine;
using System.Collections;

public class MouseDownEnable : MonoBehaviour {

	// enable or disable car rotation based on raycast hit to vehicle
	public bool rotEnabled = false;
	public GameObject script;

	void Start(){
	}

	void Update () {
		if (Input.GetButtonDown("Fire1")){
			rotEnabled = true;
		} else if (Input.GetButtonUp("Fire1")){
			rotEnabled = false;
		}
		
		GetComponent<MouseLook>().enabled = rotEnabled;
	}

	// needed because switching from third to first camera maintained active mouse look
	public void DisableMouseLook(){
		rotEnabled = false;
		Debug.Log("Disabling mouseLook");
	}
}
