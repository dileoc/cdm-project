﻿using UnityEngine;
using System.Collections;

public class ToggleCamera : MonoBehaviour {

	public Camera firstPersonCamera;
	public Camera thirdPersonCamera;
	public GameObject mouseDownScriptHolder;

	// Use this for initialization
	void Start () {
		firstPersonCamera.gameObject.SetActive(true);
		thirdPersonCamera.gameObject.SetActive(false);
	}

	public void ToggleCameraView(){
		firstPersonCamera.gameObject.SetActive(!firstPersonCamera.gameObject.activeSelf);
		thirdPersonCamera.gameObject.SetActive(!thirdPersonCamera.gameObject.activeSelf);
		mouseDownScriptHolder.SendMessage("DisableMouseLook", SendMessageOptions.DontRequireReceiver);
	}
}
