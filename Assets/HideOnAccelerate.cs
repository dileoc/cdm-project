﻿using UnityEngine;
using System.Collections;

public class HideOnAccelerate : MonoBehaviour {

	public AudioSource playOnStart;
	public float delayTime = 5f;
	public GameObject highlander;
	public GameObject instructionPopup;

	// Use this for initialization
	void Start () {
		playOnStart.PlayDelayed(delayTime);
		Invoke("showPopup", 2f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis("Vertical") > 0){
			gameObject.SetActive(false);
			instructionPopup.SetActive(false);
			highlander.GetComponent<SpeedControl>().enabled = true;
		}
	}

	void showPopup(){
		instructionPopup.SetActive(true);
	}
}
